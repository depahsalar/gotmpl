package main

import (
	"flag"
	"net/http"
	"text/template"
)

// Context is the data for template
type Context struct {
	Fruit [3]string
	Title string
}

func main() {
	var port string
	flag.StringVar(&port, "p", "80", "port number")
	flag.Parse()

	http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Add("Content-Type", "text/html")

		templates := template.New("template")
		templates.New("docs").Parse(doc)
		templates.New("header").Parse(header)
		templates.New("footer").Parse(footer)

		context := Context{
			[3]string{"Docker", "CoreOS", "rkt"},
			"Put Title in Context",
		}
		templates.Lookup("docs").Execute(w, context)
	})

	http.ListenAndServe(":"+port, nil)
} // end of main

const header = `
<!DOCTYPE html>
<html>
  <head><title>{{.}}</title></head>
`

const doc = `
{{template "header" .Title}}
  <body>
  <h2> The Future of Software</h2>
  <ul>
  {{ range .Fruit }}
     <li> {{.}} </li>
  {{end}}
  </ul>
</body>
{{template "footer"}}
`

const footer = `
</html>
`
